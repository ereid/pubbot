from types import NoneType

# Command lists
botCmd = ["!raw","!join","!part","!say","!act","!pm","!quit"]
allCmd = ["!commands","!help","!halp","!drink","!pub","!batman","!ilw","!exclaim","!chant","!hug","!stats"]

def process(nick, message, irc): # Handle all commands
	commands = library(nick, message)
	if type(commands) is not NoneType:
		for command in commands:
			irc.send(command + "\n")

def library(nick, message):
	if " PRIVMSG " in message: # Messages
		channel = message.split(" :")[0].split("PRIVMSG ")[1]
		if channel == "PubBot":
			channel = nick
		command = message.split(" :")[1].split(" ")[0]

		# Bot management commands
		if command in botCmd and nick == "Reid":
			if command == "!raw":
				return[message.split("!raw ")[1]]
			elif command == "!join":
				return["JOIN :" + message.split(command + " ")[1]]
			elif command == "!part":
				return["PART :" + message.split(command + " ")[1]]
			elif command == "!say":
				if message.split(command + " ")[1][:1] == "#":
					return["PRIVMSG " + message.split(command + " ")[1].split(" ")[0] + " :" + message.split(message.split(command + " ")[1].split(" ")[0] + " ")[1]]
				else:
					return["PRIVMSG " + channel + " :" + message.split(command + " ")[1]]
			elif command == "!act":
				if message.split(command + " ")[1][:1] == "#":
					return["PRIVMSG " + message.split(command + " ")[1].split(" ")[0] + " :" + chr(1) + "ACTION " + message.split(message.split(command + " ")[1].split(" ")[0] + " ")[1] + chr(1)]
				else:
					return["PRIVMSG " + channel + " :" + chr(1) + "ACTION " + message.split(command + " ")[1] + chr(1)]
			elif command == "!pm":
				return["PRIVMSG " + message.split(command + " ")[1].split(" ")[0] + " :" + message.split(command + " ")[1].split(" ")[1]]
			elif command == "!quit":
				if len(message.split(command)[1]) > 0:
					return ["QUIT :" + message.split(command)[1][1:]]
				else:
					return ["QUIT :User requested"]
			else:
				return []

        	# General commands
        	elif command in allCmd:
        		if command == "!commands" or command == "!help" or command == "!halp":
        			return[\
        			"PRIVMSG " + nick + " :Ordering PubBot around for complete beginners!",\
        			"PRIVMSG " + nick + " : !commands/!help - Show this listing",\
        			"PRIVMSG " + nick + " : !drink - DRINK!",\
        			"PRIVMSG " + nick + " : !pub - Give a cheer!",\
        			"PRIVMSG " + nick + " : !batman <x> - Nanananananananananananana x man!",\
				"PRIVMSG " + nick + " : !ilw <x> - Praise Innovative <whatever> Week (default: Learning)",\
				"PRIVMSG " + nick + " : !hug <x> - Hug someone/thing! (default: yourself)",\
				"PRIVMSG " + nick + " : !exclaim <chant> <x> <shout> - Says chant x times, then exclaims with a !"]
        		elif command == "!drink":
        			return["PRIVMSG " + channel + " :" + chr(1) + "ACTION takes a drink." + chr(1)]
        		elif command == "!pub":
        			return[\
				"PRIVMSG " + channel + " :TO THE PUB!",\
				"PRIVMSG " + channel + " :" + chr(1) + "ACTION goes to the pub." + chr(1)]
			elif command == "!batman":
				manType = message.split("!batman ")[1] if "!batman " in message and len(message.split("!batman")) > 1 else ""
				return["PRIVMSG " + channel + " :Nanananananananananananana " + manType + "batman!"]
			elif command == "!ilw":
				weekType = message.split("!ilw ")[1] if "!ilw " in message and len(message.split("!ilw")) > 1 else "Learning"
				return["PRIVMSG " + channel + " :Hurrah for Innovative " + weekType.strip() + " Week!"]
			elif command == "!exclaim" or command == "!chant":
				counter = 0
				messageBits = message.split(" ")
				while (not messageBits[counter].isdigit()):
					counter += 1
					if counter == len(messageBits):
						return []
				count = int(messageBits[counter])
				chant = message.split(command + " ")[1].split(" " + str(count) + " ")[0] if (" " + str(count) + " ") in message.split(command + " ")[1] else message.split(command + " ")[1].split(" " + str(count))[0]
				exclaim = "" if len(message.split(command + " ")[1].split(" " + str(count) + " ")) == 1 else " " + message.split(command + " ")[1].split(" " + str(count) + " ")[1]
				if count*len(chant)+len(exclaim)+1 > 453:
					return["PRIVMSG " + channel + " :NOPE"]
				return["PRIVMSG " + channel + " :" + chant*count + exclaim.strip() + "!"]
			elif command == "!hug":
				toHug = message.split("!hug ")[1] if "!hug " in message and len(message.split("!hug")) > 1 else nick
				return["PRIVMSG " + channel + " :" + chr(1) + "ACTION hugs " + toHug.strip() + "." + chr(1)]
			elif command == "!stats":
				return["PRIVMSG " + channel + " :Stats for #pub found at http://ironfist.thehavennet.org.uk/stats/pubstats.html"]
	   		else:
        			return []
        	# No command
        	else:
        		return []
        elif " JOIN :" in message: # Event for user joining room
		return["MODE " + message.split(" :")[1] + " +o " + message.split('!')[0][1:]]
        elif " INVITE " in message and message.split('!')[0][1:] == "Reid":
		return["JOIN :" + message.split(' :')[1]]
