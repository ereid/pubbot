# Import some necessary libraries.
import socket
import PubCommands as commandLibrary

# Global Vars - Change the first 4 to host on a new server
server = "irc.imaginarynet.org.uk"	# Server
botnick = "PubBot"			# Nick
realname = "The #Pub Bot"		# What to set as realname
ownernick = "Reid"			# Owner, i.e. who's allowed to reload the command library
ircsock = socket.socket( )		# Socket we're connecting with


# Interface with command library
def commands(nick,message):
	if " PRIVMSG " in message and nick == ownernick and message.split(message.split(' PRIVMSG ')[1].split(' :')[0] + ' :')[1] == "!reload": # if owner wants to reload, reload
		reload(commandLibrary)
		ircsock.send("PRIVMSG " + nick + " :Commands reloaded")
	else:
		commandLibrary.process(nick,message,ircsock)

# Server connection and start main loop
def startup():
	ircsock.connect((server, 6667)) # Here we connect to the server using the port 6667
	ircsock.send("USER "+ botnick +" "+ botnick +" "+ botnick +" :" + realname + "\n") # user authentication
	ircsock.send("NICK "+ botnick +"\n") # here we actually assign the nick to the bot
	mainloop()

# Main loop (seperate in case the bot crashes)
def mainloop(): # Run, bot, run!
	while 1: # Main (infinite) loop
		ircmsg = ircsock.recv(2048) # receive and parse data from the server
		ircmsg = ircmsg.strip('\n\r')
		print(ircmsg) # Server log in case we need to debug
		if "PING :" in ircmsg: # Reply to pings
			ircsock.send("PONG :" + ircmsg.split(':')[1] + "\n")
		else: # Pass everything else to the command handler
			nick = ircmsg.split('!')[0][1:]
			commands(nick,ircmsg)

startup()
